$(document).ready(function () {
    let nav_html = "";

    create_nav_block();

    function create_nav_block() {
        $.each($(".main_block"), function (num, dom) {
            const dom_id = $(dom).attr("id");

            nav_html += "<input type='radio' name='nav_input'>"
            nav_html += "<a class='nav_item' href='#" + dom_id + "'>";
            nav_html += "<img src='image/icon/" + dom_id + ".png'></a>";
        });

        $("#resume_container>nav").html(nav_html);
        $("#resume_container>nav>input:first").prop("checked", true);
    }

    // $(document).on("click", "a.nav_item", function () {
    //     const scroll_top = $(".main_block" + $(this).attr("href")).offset().top;

    //     $("body, html").animation({ scrollTop: scroll_top }, 600);
    // });
});